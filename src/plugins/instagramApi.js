import axios from "axios";
import InstagramHelper from "../helpers/instagram.helper.js";

class InstagramApi {
    session_id = '';
    api = {
        base_url: 'https://i.instagram.com/api',
        v: 'v1',
        query: { __a: 1, __d: 'dis' },
        query_str: '?__a=1&__d=dis',
        query_hash: 'd4d88dc1500312af6f937f7b804c68c3',
    };
    axios = axios.create({
        baseURL: this.axiosBaseUrl,
        timeout: 5000,
        headers: {
            "accept": "*/*",
            "x-ig-app-id": "936619743392459",
            "accept-language": "ru,en-US;q=0.9,en;q=0.8,ru-RU;q=0.7,hy;q=0.6,de;q=0.5",
        },
    });
    rejects = {
        session_err: { code: 501, msg: 'Для получение информации необходимо указать параметр ' },
        other_err: { code: 500, msg: 'Неизвестная ошибка' },
        not_found: { code: 404, msg: 'Ничего не найдено' },
        user_not_found: { code: 404, msg: 'Пользователь не найден' },
        post_not_found: { code: 404, msg: 'Публикация не найдена' },
        posts_not_found: { code: 404, msg: 'Нет публикации' },
        stories_not_found: { code: 404, msg: 'Нет истории' },
        story_not_found: { code: 404, msg: 'История не найдена' },
    };
    error_codes = [404, 500, 501];

    constructor(session_id = '', opt = {}) {
        this.setSessionId(session_id);
        this.setHeadersBaseUrl();
    }

    setSessionId(session_id) {
        this.sessionId = session_id;
        this.setHeadersSessionId(session_id);
    }
    setHeadersSessionId(session_id) {
        this.axios.defaults.headers['cookie'] = `sessionid=${session_id}`;
    }
    setHeadersBaseUrl() {
        this.axios.defaults.baseURL = this.axiosBaseUrl;
    }

    async getUserByUsername(username) {
        let url = `/users/web_profile_info/?username=${username}`;
        return await new Promise(async (resolve, reject) => {
            await this.axiosGet(url)
                .then(data => (data.data.user !== undefined && data.data.user !== null) ? resolve(data.data.user) : reject(this.rejects.other_err))
                .catch(err => reject(err.status === 404 ? this.rejects.user_not_found : err))
        });
    }
    async getUserId(username) {
        return await new Promise(async (resolve, reject) => {
            await this.getUserByUsername(username).then(user => reject(user.id)).catch(reject)
        })
    }
    async getUserAndOther(username, other = {}) {
        return await new Promise(async (resolve, reject) => {
            let errors = [];
            await this.getUserByUsername(username)
                .then(async user => {
                    if (other.posts) await this.getUserPosts(user.id, 10).then(posts => {
                        this.setPostsToUser(user, posts);
                    }).catch(err => errors.push(err));
                    else if (other.all_posts) await this.getUserAllPosts(user.id).then(posts => {
                        this.setPostsToUser(user, posts);
                    }).catch(err => errors.push(err));
                    if (other.stories) await this.getUserStories(user.id).then(stories => {
                        this.setStoriesToUser(user, stories);
                    }).catch(err => errors.push(err));
                    user.errors = errors;
                    resolve(user);
                })
                .catch(reject);
        });
    }
    async getUserAndAllPosts(username) {
        let user = await this.getUserByUsername(username).catch(err => {});
        let posts = await this.getUserAllPosts(user.id).catch(err => {});
        user.posts = posts.items;
        user.posts_options = {
            num_results: posts.num_results || null,
            more_available: posts.more_available || false,
            next_max_id: posts.next_max_id || '',
        };
        return user;
    }
    async getUserAndLastPosts(username) {
        let user = await this.getUserByUsername(username).catch(err => {}) || null;
        if (this.error_codes.indexOf(user.code) >= 0) return user;
        let posts = await this.getUserPosts(user.id, 10).catch(err => {});
        user.posts = posts.items || [];
        user.posts_options = {
            num_results: posts.num_results || null,
            more_available: posts.more_available || false,
            next_max_id: posts.next_max_id || '',
        };
        return user;
    }
    async getUserAndAllStories(username) {
        let user = await this.getUserByUsername(username).catch(err => {}) || null;
        if (!user) return this.rejects.user_not_found;
        let stories = await this.getUserStories(user.id).catch(err => {}) || null;
        // if (!stories) return this.rejects.other_err;
        if (stories !== null && stories.items.length) user.stories = stories.items;
        return user;
    }

    async getUserPosts(userId, count = 10, max_id = '') {
        let url = `/feed/user/${userId}?count=${count}`;
        // /feed/user/anjelikaxechikyan/username/?count=12
        // https://www.instagram.com/api/v1/feed/user/okay_susann/username/?count=12
        if (max_id.length) url += `&max_id=${max_id}`;
        return await new Promise((resolve, reject) => {
            this.axiosGet(url)
                .then(data => {
                    return data.items.length ? resolve({
                        items: data.items,
                        num_results: data.num_results,
                        more_available: data.more_available,
                        next_max_id: data.next_max_id || '',
                        user: data.user,
                    }) : reject(this.rejects.posts_not_found);
                })
                .catch(err => reject(err.status === 404 ? this.rejects.user_not_found : err));
        })
    }
    async getUserAllPosts(userId, items = [], more_available = false, num_results = 0, next_max_id = '', user = {}, i = 0) {
        let posts = await this.getUserPosts(userId, 30, next_max_id).catch(err => {}) || {};
        const respData = { items: [...items, ...posts.items], num_results, more_available, next_max_id, user };
        // console.log(i, more_available, next_max_id);
        if (posts.more_available) return this.getUserAllPosts(userId, respData.items, posts.more_available, posts.num_results, posts.next_max_id, posts.user, i + 1);
        else return respData;
    }

    async getPostById(postId) {
        let url = `/media/${postId}/info`;
        return await new Promise(async (resolve, reject) => {
            if (!this.sessionId.length) return reject(this.rejects.session_err);
            await this.axiosGet(url)
                .then(data => data.items.length ? resolve(data.items[0]) : reject(this.rejects.other_err))
                .catch(err => reject((err.status === 404 || err.status === 400) ? this.rejects.post_not_found : err))
        })
    }
    async getPostByCode(postCode) {
        let postId = InstagramHelper.posShortcodeToId(postCode);
        return this.getPostById(postId);
    }

    async getUserStories(userId) {
        let url = `/feed/reels_media/?reel_ids=${userId}`;
        return await new Promise((resolve, reject) => {
            this.axiosGet(url)
                .then(data => {
                    // console.log(data);
                    let no_media = { items: [], media_count: 0, ...this.rejects.stories_not_found };
                    if (!(data['reels_media'] || []).length) return resolve(no_media);
                    if (!(data['reels_media'][0]['items'] || []).length) return resolve(no_media);
                    let reels_media = data['reels_media'][0];
                    let user = reels_media.user || {};
                    let items = (reels_media.items || []).map(item => ({...item, user }));
                    resolve({
                        items,
                        media_count: reels_media.num_results || 0,
                        media_ids: reels_media.media_ids || [],
                        expiring_at: reels_media.expiring_at || '',
                        seen: reels_media.seen || 0,
                        reel_type: reels_media.reel_type || '',
                        user,
                    });
                })
                .catch(err => reject(err.status === 404 ? this.rejects.user_not_found : err))
        })
    }

    async getUserHighlight(highlightId) {
        let url = `/feed/reels_media/?reel_ids=highlight%3A${highlightId}`;
        return await new Promise((resolve, reject) => {
            this.axiosGet(url)
                .then(data => {
                    // console.log(data);
                    let no_media = { items: [], media_count: 0, ...this.rejects.stories_not_found };
                    if (!(data['reels_media'] || []).length) return resolve(no_media);
                    if (!(data['reels_media'][0]['items'] || []).length) return resolve(no_media);
                    let reels_media = data['reels_media'][0];
                    let user = reels_media.user || {};
                    let items = (reels_media.items || []).map(item => ({...item, user }));
                    resolve({
                        items,
                        media_count: reels_media.num_results || 0,
                        media_ids: reels_media.media_ids || [],
                        expiring_at: reels_media.expiring_at || '',
                        seen: reels_media.seen || 0,
                        reel_type: reels_media.reel_type || '',
                        user,
                    });
                })
                .catch(err => reject(err.status === 404 ? this.rejects.user_not_found : err))
        })
    }


    setPostsToUser(user, posts = {}) {
        if (posts !== null && posts.items.length) {
            user.posts = posts.items;
            user.posts_options = {
                num_results: posts.num_results || null,
                more_available: posts.more_available || false,
                next_max_id: posts.next_max_id || '',
            };
        }
        return user;
    }
    setStoriesToUser(user, stories = {}) {
        if (stories !== null && stories.items.length) {
            user.stories = stories.items;
            user.stories_options = {
                media_count: stories.num_results || 0,
                media_ids: stories.media_ids || [],
                expiring_at: stories.expiring_at || '',
                seen: stories.seen || 0,
                reel_type: stories.reel_type || '',
            };
        }
        return user;
    }

    async axiosGet(url = '', body = null) {
        return await new Promise((resolve, reject) => {
            this.axios.get(url)
                .then(({ data }) => {
                    // console.log(data);
                    resolve(data);
                })
                .catch(err => {
                    if (err.response) return reject(err.response)
                    else if (err.request) return reject(err.request)
                    else return reject(err.message);
                });
        })
    }
    async axiosPost(url = '', body = null, headers) {
        return await new Promise((resolve, reject) => {
            this.axios.post(url, body, { headers })
                .then(({ data }) => {
                    // console.log(data);
                    resolve(data);
                })
                .catch(err => {
                    if (err.response) return reject(err.response)
                    else if (err.request) return reject(err.request)
                    else return reject(err.message);
                });
        })
    }

    set sessionId(sessionId) {
        this.session_id = (sessionId || '').trim() || '';
    }
    get sessionId() {
        return this.session_id || '';
    }

    set apiBaseUrl(baseUrl) {
        let base_url = (baseUrl || '').trim();
        if (!base_url.length) return false;
        this.api.base_url = base_url;
        this.setHeadersBaseUrl();
    }
    get apiBaseUrl() {
        return this.api.base_url || '';
    }
    set apiV(version) {
        let v = (version || '').trim();
        if (!v.length) return false;
        this.api.v = v;
        this.setHeadersBaseUrl();
    }
    get apiV() {
        return this.api.v || '';
    }

    get axiosBaseUrl() {
        return `${this.apiBaseUrl}/${this.apiV}`;
    }

}

export default InstagramApi;
