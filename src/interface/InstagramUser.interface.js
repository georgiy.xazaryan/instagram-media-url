import InstagramPostInterface from "./InstagramPost.interface.js";
import InstagramStoryInterface from "./InstagramStory.interface.js";

export default class InstagramUserInterface {
    id = '';
    pk = '';
    fbid = '';
    username = '';
    full_name = '';
    profile_pic_url = '';
    profile_pic_url_hd = '';
    edge_followed_by = { count: null }; // Подписчики
    edge_follow = { count: null }; // Подписки
    edge_owner_to_timeline_media = {
        count: null
    } // Публикации

    is_private = false;
    is_verified = false;

    blocked_by_viewer = false;
    requested_by_viewer = false;
    restricted_by_viewer = false;
    followed_by_viewer = false;
    follows_viewer = false;
    has_blocked_viewer = false;
    has_requested_viewer = false;
    is_guardian_of_viewer = false;
    is_supervised_by_viewer = false;
    is_supervised_user = false;
    hide_like_and_view_counts = false;
    biography = '';
    bio_links = [];
    connected_fb_page = null;
    ig_data = {};
    posts = [];
    posts_options = {
        num_results: null,
        more_available: false,
        next_max_id: '',
    };
    stories = [];
    stories_options = {
        media_count: 0,
        media_ids: [],
        expiring_at: '',
        seen: 0,
        reel_type: '',
    }
    errors = [];

    constructor(data = {}) {
        this.setData(data);
    }

    setData(data) {
        if (data.id) this.id = data.id;
        if (data.pk) this.pk = data.pk;
        if (data.fbid) this.fbid = data.fbid;
        if (data.username) this.username = data.username;
        if (data.full_name) this.full_name = data.full_name;
        if (data.profile_pic_url) this.profile_pic_url = data.profile_pic_url;
        if (data.profile_pic_url_hd) this.profile_pic_url_hd = data.profile_pic_url_hd;
        if (data.edge_followed_by) this.edge_followed_by = data.edge_followed_by;
        if (data.edge_follow) this.edge_follow = data.edge_follow;
        if (data.edge_owner_to_timeline_media) this.edge_owner_to_timeline_media = data.edge_owner_to_timeline_media;
        if (data.is_private) this.is_private = data.is_private;
        if (data.is_verified) this.is_verified = data.is_verified;
        if (data.blocked_by_viewer) this.blocked_by_viewer = data.blocked_by_viewer;
        if (data.requested_by_viewer) this.requested_by_viewer = data.requested_by_viewer;
        if (data.restricted_by_viewer) this.restricted_by_viewer = data.restricted_by_viewer;
        if (data.followed_by_viewer) this.followed_by_viewer = data.followed_by_viewer;
        if (data.follows_viewer) this.follows_viewer = data.follows_viewer;
        if (data.has_blocked_viewer) this.has_blocked_viewer = data.has_blocked_viewer;
        if (data.has_requested_viewer) this.has_requested_viewer = data.has_requested_viewer;
        if (data.is_guardian_of_viewer) this.is_guardian_of_viewer = data.is_guardian_of_viewer;
        if (data.is_supervised_by_viewer) this.is_supervised_by_viewer = data.is_supervised_by_viewer;
        if (data.is_supervised_user) this.is_supervised_user = data.is_supervised_user;
        if (data.hide_like_and_view_counts) this.hide_like_and_view_counts = data.hide_like_and_view_counts;
        if (data.biography) this.biography = data.biography;
        if (data.bio_links) this.bio_links = data.bio_links;
        if (data.connected_fb_page) this.connected_fb_page = data.connected_fb_page;
        // this.ig_data = data.ig_data;
        // Object.keys(data).forEach(key => this[key] = data[key]);
        if (data.posts) this.setPosts(data.posts);
        if (data.stories) this.setStories(data.stories);
        if (data.posts_options) this.setPostsOptions(data.posts_options);
        if (data.stories_options) this.setStoriesOptions(data.stories_options);


        if (data.errors) this.errors = data.errors;
    }
    setPosts(posts = [], destroy = true) {
        if (destroy) this.posts = [];
        this.posts = posts.map(post => post instanceof InstagramPostInterface ? post : new InstagramPostInterface(post));
    }
    setPostsAndPostsData(posts = {}, destroy = true) {
        if (posts !== null && posts.items.length) {
            this.setPosts(posts.items, destroy);
            this.setPostsOptions({
                num_results: posts.num_results || null,
                more_available: posts.more_available || false,
                next_max_id: posts.next_max_id || '',
            });
        }
    }
    addPost(post) {
        this.posts.push(post instanceof InstagramPostInterface ? post : new InstagramPostInterface(post));
    }

    setStories(stories = [], destroy = true) {
        if (destroy) this.stories = [];
        this.stories = stories.map(story => story instanceof InstagramStoryInterface ? story : new InstagramStoryInterface(story));
    }
    setStoriesAndStoriesData(stories = {}, destroy = true) {
        if (stories !== null && stories.items.length) {
            this.setStories(stories.items, destroy);
            this.setStoriesOptions({
                media_count: stories.num_results || 0,
                media_ids: stories.media_ids || [],
                expiring_at: stories.expiring_at || '',
                seen: stories.seen || 0,
                reel_type: stories.reel_type || '',
            });
        }
    }
    addStory(story) {
        this.stories.push(story instanceof InstagramStoryInterface ? story : new InstagramStoryInterface(story));
    }

    setPostsOptions(options = {}) {
        this.posts_options = Object.assign({}, options);
    }
    setStoriesOptions(options = {}) {
        this.stories_options = Object.assign({}, options);
    }

    findStoryById(storyId) {
        return this.stories.find(story => story.postId === storyId) || null;
    }

    get userId() {
        return this.id;
    }
    get profilePic() {
        return this.profilePicUrlHd.length ? this.profilePicUrlHd : this.profilePicUrl;
    }
    get profileName() {
        return this.profileFullName.length ? this.profileFullName : this.profileUsername;
    }
    get allPostsMediaUrls() {
        return this.posts.map(post => post.allMediaUrls).flat();
    }
    get allPostsMediaObjItems() {
        return this.posts.map(post => post.allMediaObjItems).flat();
    }
    get allStoriesMediaObjItems() {
        return this.stories.map(story => story.allMediaObjItems).flat();
    }
    get allStoriesIds() {
        return this.stories.map(story => story.postId);
    }
    get allStoriesUrls() {
        return this.stories.map(story => story.allMediaUrls).flat();
    }
    get allUserMediaObjItems() {
        return [...this.allPostsMediaObjItems, this.allStoriesMediaObjItems].flat();
    }

    get profilePicUrlHd() {
        return this.profile_pic_url_hd || '';
    }
    get profilePicUrl() {
        return this.profile_pic_url || '';
    }
    get profileFullName() {
        return this.full_name || '';
    }
    get profileUsername() {
        return this.username || '';
    }

    get allPostsCount() {
        return this.edge_owner_to_timeline_media.count || 0;
    }
    get storiesCount() {
        return this.stories.length || 0;
    }

    get hasAccess() {
        if (this.blocked_by_viewer) return false;
        if (this.is_private) return this.followed_by_viewer;
        return true;
    }
}
