import InstagramUserInterface from "./InstagramUser.interface.js";

export default class InstagramPostInterface {
    id = '';
    pk = '';
    code = '';
    media_type = null;
    filter_type = null;
    caption = '';
    accessibility_caption = '';

    like_and_view_counts_disabled = false;
    comment_count = null;
    comments = [];
    like_count = null;
    has_liked = false;
    taken_at = null;

    album = [];
    photo = {
        src: '',
        width: null,
        height: null,
    };
    video = {
        src: '',
        width: null,
        height: null,
        duration: null,
        type: null,
        poster: {
            src: '',
            width: null,
            height: null,
        },
        thumbnails: {},
    };

    user = new InstagramUserInterface;

    media_type_title = '';
    media_type_type = '';

    post_type = 'post';

    constructor(data = {}) {
        this.setData(data);
    }
    setData(data) {
        if (data.id) this.id = data.id;
        if (data.pk) this.pk = data.pk;
        if (data.code) this.code = data.code;
        if (data.media_type) this.media_type = data.media_type;
        if (data.filter_type) this.filter_type = data.filter_type;
        if (data.caption) this.caption = data.caption;
        if (data.accessibility_caption) this.accessibility_caption = data.accessibility_caption;
        if (data.like_and_view_counts_disabled) this.like_and_view_counts_disabled = data.like_and_view_counts_disabled;
        if (data.comment_count) this.comment_count = data.comment_count;
        if (data.comments) this.comments = data.comments;
        if (data.like_count) this.like_count = data.like_count;
        if (data.has_liked) this.has_liked = data.has_liked;
        if (data.taken_at) this.taken_at = data.taken_at;

        if (data.user) this.user.setData(data.user);

        if (data.post_type) this.post_type = data.post_type;

        this.checkMediaType(data);



        // Object.keys(data).forEach(key => this[key] = data[key]);
    }
    checkMediaType(data) {
        const media_type = data.media_type;
        const media_types = [1, 2, 8];
        const media_types_title = {
            1: 'Фото',
            2: 'Видео',
            8: 'Альбом',
        };
        const media_type_types = {
            1: 'photo',
            2: 'video',
            8: 'album',
        };
        this.media_type_title = media_types_title[media_type];
        this.media_type_type = media_type_types[media_type];
        switch (media_type) {
            case 1: this.setPhoto(data['image_versions2']['candidates'], data['original_width'], data['original_height']); break;
            case 2: this.setVideo(data['video_versions'], data['original_width'], data['original_height'], data['video_duration'], data['image_versions2']['candidates'], data['thumbnails']); break;

            case 8: this.setAlbum(data['carousel_media']); break;
        }
    }
    checkPhoto(images = [], original_w = 1080, original_h = 1080) {
        let image = images.find(img => (img.width === original_w && img.height === original_h)) || images[0];
        return {
            src: image.url,
            width: image.width,
            height: image.height,
        }
    }
    checkVideo(videos = [], original_w = 1080, original_h = 1080, duration = 0, posters = [], thumbnail = {}) {
        let video = videos.find(vid => (vid.width === original_w && vid.height === original_h)) || videos[0];
        let poster = posters.find(img => (img.width === original_w && img.height === original_h)) || posters[0];
        return {
            src: video.url,
            width: video.width,
            height: video.height,
            duration: +duration,
            type: video.type,
            poster: {
                src: poster.url,
                width: poster.width,
                height: poster.height,
            },
            // thumbnails: {},
        };
    }

    setPhoto(images = [], original_w = 1080, original_h = 1080) {
        this.photo = this.checkPhoto(images, original_w, original_h);
    }
    setVideo(videos = [], original_w = 1080, original_h = 1080, duration = 0, posters = [], thumbnail = {}) {
        this.video = this.checkVideo(videos, original_w, original_h, duration, posters, thumbnail);
    }
    setAlbum(carousel_media = []) {
        const media_type_types = {
            1: 'photo',
            2: 'video',
            8: 'album',
        };
        carousel_media.forEach(media => {
            let album_item = {
                media_type: media.media_type,
                media_type_type: media_type_types[media.media_type],
                src: '',
                photo: { src: '', width: null, height: null },
                video: { src: '', width: null, height: null, duration: null, type: null, poster: { src: '', width: null, height: null }, thumbnails: {} },
            };
            switch (media.media_type) {
                case 1:
                    album_item.photo = this.checkPhoto(media['image_versions2']['candidates'], media['original_width'], media['original_height']);
                    album_item.src = album_item.photo.src;
                    break;
                case 2:
                    album_item.video = this.checkVideo(media['video_versions'], media['original_width'], media['original_height'], media['video_duration'], media['image_versions2']['candidates'], media['thumbnails']);
                    album_item.src = album_item.video.src;
                    break;
            }
            this.album.push(album_item);
        })
    }



    get postId() {
        return (this.pk || '').length ? this.pk : this.id.split('_')[0];
    }

    get mediaObj() {
        return {
            post_id: this.postId ?? '',
            post_type: this.post_type,
            media_id: '',
            type: this.isVideo ? 'video' : 'image',
            file_type: this.isVideo ? 'mp4' : 'jpg',
            name: this.postId || '',
            file_full_name: `${this.postId || ''}.${this.isVideo ? 'mp4' : 'jpg'}`,
            url: this.isPhoto ? this.photoUrl : this.isVideo ? this.videoUrl : '',
        }
    }
    get isPhoto() {
        return this.media_type_type === 'photo';
    }
    get isVideo() {
        return this.media_type_type === 'video';
    }
    get isAlbum() {
        return this.media_type_type === 'album';
    }

    get photoUrl() {
        return this.photo.src;
    }
    get videoUrl() {
        return this.video.src;
    }
    get albumItemsUrls() {
        return this.album.map(item => item.src);
    }
    get albumMediaObjItems() {
        return this.album.map((item, i) => ({
            ...this.mediaObj,
            post_id: this.postId ?? '',
            post_type: this.post_type,
            media_id: (i + 1).toString(),
            type: item.media_type_type,
            file_type: item.media_type_type === 'video' ? 'mp4' : 'jpg',
            name: `${this.postId || ''}_${(i + 1).toString()}`,
            file_full_name: `${this.postId || ''}_${(i + 1).toString()}.${item.media_type_type === 'video' ? 'mp4' : 'jpg'}`,
            url: item.src,
        }));
    }

    get allMediaUrls() {
        if (this.isPhoto) return [this.photoUrl];
        if (this.isVideo) return [this.videoUrl];
        if (this.isAlbum) return this.albumItemsUrls;
        return [];
    }
    get allMediaObjItems() {
        if (this.isPhoto || this.isVideo) return [this.mediaObj];
        if (this.isAlbum) return this.albumMediaObjItems;
        return [];
    }
}
