import InstagramPostInterface from "./InstagramPost.interface.js";

export default class InstagramStoryInterface extends InstagramPostInterface {
    // post_type = 'story';
    constructor(data = {}) {
        super({ ...data, post_type: 'story' });
    }
}
// export default class InstagramStoryInterface {
//     // post_type = 'story';
//     constructor(data = {}) {
//
//     }
// }
