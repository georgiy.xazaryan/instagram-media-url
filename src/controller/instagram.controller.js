import NodeCache from "node-cache";
import InstagramApi from "../plugins/instagramApi.js";
import InstagramUserInterface from "../interface/InstagramUser.interface.js";
import InstagramPostInterface from "../interface/InstagramPost.interface.js";
import InstagramStoryInterface from "../interface/InstagramStory.interface.js";
import instConfig from "../../config/inst.config.js";

export default class InstagramController {
    instagramLib = new InstagramApi(instConfig.sessionId);
    instagramCache = new NodeCache({ stdTTL: 60 * 5 });
    in_process = false;
    constructor() {}

    async checkInstUrl(url = '') {
        return await new Promise(async (resolve, reject) => {
            const instUrl = new URL(url.replace('/reel/', '/p/'));
            let href = instUrl.href, pathname = instUrl.pathname;
            if (pathname.startsWith('/p/')) return await this.findInstPost(href).then(post => {
                resolve(post.allMediaUrls)
            }).catch(reject);
            if (pathname.indexOf('/highlights/') >= 0) return this.findInstHighlights(pathname).then(stories => {
                let urls = stories.map(story => story.allMediaUrls).flat();
                resolve(urls)
            }).catch(reject)
            if (pathname.startsWith('/stories/')) return this.findInstStory(pathname).then(story => {
                resolve(story.allMediaUrls)
            }).catch(reject)
            this.findInstUser(href).then(user => {
                resolve({
                    posts: user.allPostsMediaUrls,
                    stories: user.allStoriesUrls,
                })
            }).catch(reject)
        })
    }

    async findInstPost(urlOrCode) {
        return await new Promise(async (resolve, reject) => {
            let code = this.checkFindInstPostCode(urlOrCode);
            await this.getInstPost(code).then(post => {
                resolve(post);
            }).catch(err => {
                reject(err);
                console.log('[findInstPost err]', err);
            })
        })
    }
    checkFindInstPostCode(urlOrCode = '') {
        let code = urlOrCode;
        let urlRegex = /^https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)$/;
        if (urlRegex.test(urlOrCode)) try {
            code = new URL(urlOrCode).pathname.split('/')[2] || '';
        } catch (e) {
            return '';
        }
        return code;
    }
    async getInstPost(code) {
        return await new Promise((resolve, reject) => {
            this.instagramLib.getPostByCode(code).then(post => resolve(new InstagramPostInterface(post))).catch(reject);
        })
    }


    async findInstStory(pathname = '') {
        let [ empty, str, username, storyId ] = pathname.split('/');
        return await new Promise(async (resolve, reject) => {
            await this.getInstUserAndOther(username, { stories: true })
                .then(user => {
                    let story = user.findStoryById(storyId) || null;
                    story ?  resolve(story) : reject({})
                })
                .catch(reject)
        })
    }
    async getInstUserAndOther(username, other = {}) {
        return await new Promise(async (resolve, reject) => {
            await this.instagramLib.getUserAndOther(username, other)
                .then(user => {
                    let userObj = new InstagramUserInterface(user);
                    resolve(userObj);
                })
                .catch(reject);
        })
    }

    async findInstHighlights(pathname = '') {
        let [ empty, str, highlightsStr, highlightId ] = pathname.split('/');
        return await new Promise(async (resolve, reject) => {
            await this.instagramLib.getUserHighlight(highlightId)
                .then(highlights => {
                    let stories = (highlights?.items || []).map(item => new InstagramStoryInterface(item));
                    resolve(stories);
                })
                .catch(reject)
        })
    }

    async findInstUser(urlOrUsername = '') {
        return await new Promise(async (resolve, reject) => {
            let username = this.checkFindInstUserUsername(urlOrUsername);
            await this.getInstUserAndOther(username, { posts: true, stories: true })
                .then(user => resolve(user))
                .catch(reject)
        })
    }
    checkFindInstUserUsername(urlOrUsername = '') {
        let username = urlOrUsername;
        let urlRegex = /^https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)$/;
        if (urlRegex.test(urlOrUsername)) try {
            username = new URL(urlOrUsername).pathname.replace(/[\/]/g, '');
        } catch (e) {
            return ''
        }
        return username;
    }
}
