const BUTTONS = {
    instagram: {
        label: 'Инстаграм',
        command: '/instagram'
    },

    instagram_guest: {
        label: '👀 IG Гость',
        command: '/instagram_guest'
    },
    instagram_add_account: {
        label: '➕ IG Добавить аккаунт',
        command: '/instagram_add_account'
    },
    instagram_change_account: {
        label: '🔄 IG Сменить аккаунт',
        command: '/instagram_change_account'
    },
    instagram_my_search: {
        label: 'IG Поиск',
        command: '/instagram_my_search'
    },



    go_home: {
        label: '⬅️ На главную',
        command: '/home'
    },


    back: {
        label: '🔙 Назад',
        command: '/back'
    },

}
export default BUTTONS;
