import bigInt from "big-integer";
const InstagramHelper = {
    variables() {
        let lower = 'abcdefghijklmnopqrstuvwxyz';
        let upper = lower.toUpperCase();
        let numbers = '0123456789';
        let ig_alphabet =  upper + lower + numbers + '-_';
        let bigint_alphabet = numbers + lower;
        return { lower, upper, numbers, ig_alphabet, bigint_alphabet };
    },
    postIdToShortcode(longId) {
        const variables = this.variables();
        let o = bigInt(longId).toString(64);
        return o.replace(/<(\d+)>|(\w)/g, (m, m1, m2) => {
            return variables.ig_alphabet.charAt((m1) ? parseInt(m1) : variables.bigint_alphabet.indexOf(m2))
        });
    },
    posShortcodeToId(shortCode) {
        const variables = this.variables();
        let o = shortCode.replace( /\S/g, m => {
            let c = variables.ig_alphabet.indexOf(m);
            let b = variables.bigint_alphabet.charAt(c);
            return (b !== '') ? b : `<${c}>`;
        })
        return bigInt(o, 64).toString(10);
    }
}
export default InstagramHelper;
