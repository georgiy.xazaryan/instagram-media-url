import express from "express";
import path from "path"
import InstagramController from "../controller/instagram.controller.js";
const indexRouter = express.Router();
const __dirname = path.resolve();
const instagramController = new InstagramController()


indexRouter.get('/instagram', async (req, res, next) => {
    let url = req.query.url || '';
    await instagramController.checkInstUrl(url)
        .then(urls => {
            res.send({ ok: true, urls });
        })
        .catch(err => {
            res.send(err);
        })
});


export default indexRouter;
